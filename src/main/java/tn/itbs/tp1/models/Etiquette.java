package tn.itbs.tp1.models;
import org.springframework.data.annotation.Id;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;


@Entity

public class Etiquette {
	@Id
	private Long id;
	private String libelle;
	@ManyToOne
	private Tache tache;
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	
	

}
