package tn.itbs.tp1.models;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;

import java.sql.Date;
import org.springframework.data.annotation.Id;

@Entity
public class Projet {
	@Id
	private Long id ;
	private String nom;
	private String description;
	private Date dateDebut;
	private Date dateFin;
	@ManyToOne
	private Utilisateur utilisateur;
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDateDebut() {
		return dateDebut;
	}
	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}
	public Date getDateFin() {
		return dateFin;
	}
	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}
	
	
	
	

}
