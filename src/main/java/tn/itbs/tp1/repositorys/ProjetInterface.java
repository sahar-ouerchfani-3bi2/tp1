package tn.itbs.tp1.repositorys;
import org.springframework.data.jpa.repository.JpaRepository;

import tn.itbs.tp1.models.Projet;

public interface ProjetInterface  extends JpaRepository<Projet, Long>{

}
