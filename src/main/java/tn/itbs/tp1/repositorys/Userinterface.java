package tn.itbs.tp1.repositorys;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

import tn.itbs.tp1.models.Utilisateur;

public interface Userinterface  extends JpaRepository<Utilisateur, Long>{
	
	List<Utilisateur> findBYNOM ( String nom);
	List<Utilisateur> findBYPrenom ( String prenom);
	List<Utilisateur> findBYEmail ( String email);
	

}
