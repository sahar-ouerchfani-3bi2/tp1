package tn.itbs.tp1.repositorys;
import org.springframework.data.jpa.repository.JpaRepository;

import tn.itbs.tp1.models.Etiquette;
public interface EtiquetteInterface extends JpaRepository<Etiquette, Long> {

}
