package tn.itbs.tp1.repositorys;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import tn.itbs.tp1.models.Tache;
public interface TacheInterface extends JpaRepository<Tache, Long>{
	List<Tache> findByProjetId (Long projetId);
}
